#ifndef DATA_STRUCTURES_H
#define DATA_STRUCTURES_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define NOVAL __builtin_inff()

#define DECLARE_DATA_STRUCTURES(T) typedef struct\
{\
    uint32_t size;\
    uint32_t capacity;\
    T* data;\
} array_##T;\
array_##T* array_##T##_create(uint32_t initial_capacity);\
void array_##T##_free(array_##T** array);\
T array_##T##_push_back(array_##T* array, T value);\
T array_##T##_pop_back(array_##T* array);\
typedef struct\
{\
    uint32_t size;\
    T* data;\
} tuple_##T;\
typedef struct node_##T\
{\
    struct node_##T* prev;\
    struct node_##T* next;\
    T value;\
} node_##T;\
typedef struct\
{\
    node_##T* first;\
    node_##T* last;\
    uint32_t size;\
} list_##T;\
node_##T* node_##T##_create(node_##T* prev, node_##T* next, T value);\
list_##T* list_##T##_create(node_##T* first, node_##T* last, uint32_t size);\
void node_##T##_free(node_##T* node);\
void list_##T##_free(list_##T* list);\
T list_##T##_push_back(list_##T* list, T value);\
T list_##T##_pop_back(list_##T* list);\
T list_##T##_push_front(list_##T* list, T value);\
T list_##T##_pop_front(list_##T* list);\
T list_##T##_insert(list_##T* list, uint32_t idx, T value);\
T list_##T##_remove(list_##T* list, uint32_t idx);\
T list_##T##_get(list_##T* list, uint32_t idx);\



#define DEFINE_DATA_STRUCTURES(T) array_##T* array_##T##_create(uint32_t initial_capacity)\
{\
    array_##T* array = (array_##T*)malloc(sizeof(array_##T));\
    array->size = 0;\
    array->capacity = initial_capacity;\
    array->data = (T*)malloc(sizeof(T) * initial_capacity);\
    return array;\
}\
void array_##T##_free(array_##T** array)\
{\
    if (array == NULL || *array == NULL)\
        return;\
    free((*array)->data);\
    free(*array);\
    *array = NULL;\
}\
T array_##T##_push_back(array_##T* array, T value)\
{\
    if (array == NULL)\
        return (T)NOVAL;\
    array->size++;\
    if (array->size > array->capacity)\
    {\
        array->capacity *= 2;\
        array->data = (T*)realloc(array->data, sizeof(T) * array->capacity);\
    }\
    array->data[array->size - 1] = value;\
    return value;\
}\
T array_##T##_pop_back(array_##T* array)\
{\
    if (array == NULL)\
        return (T)NOVAL;\
    array->size--;\
    T pop_val = array->data[array->size];\
    if (array->size <= array->capacity / 4 && array->capacity >= 8)\
    {\
        array->capacity /= 2;\
        array->data = (T*)realloc(array->data, sizeof(T) * array->capacity);\
    }\
    array->data[array->size] = (T)0;\
    return pop_val;\
}\
node_##T* node_##T##_create(node_##T* prev, node_##T* next, T value)\
{\
    node_##T* new_node = (node_##T*)malloc(sizeof(node_##T));\
    new_node->next = next;\
    new_node->prev = prev;\
    new_node->value = value;\
    return new_node;\
}\
list_##T* list_##T##_create(node_##T* first, node_##T* last, uint32_t size)\
{\
    list_##T* new_list = (list_##T*)malloc(sizeof(list_##T));\
    new_list->first = first;\
    new_list->last = last;\
    new_list->size = size;\
    return new_list;\
}\
void node_##T##_free(node_##T* node)\
{\
    if (node == NULL)\
        return;\
    free(node);\
}\
void list_##T##_free(list_##T* list)\
{\
    if (list == NULL)\
        return;\
    node_##T* current_node = list->first;\
    node_##T* next_node = list->first->next;\
    do {\
        node_##T##_free(current_node);\
        current_node = next_node;\
        next_node = current_node->next;\
    } while(next_node->next != NULL);\
    free(list);\
}\
T list_##T##_push_back(list_##T* list, T value)\
{\
    if (list == NULL)\
        return (T)NOVAL;\
    node_##T* new_node = node_##T##_create(list->last, NULL, value);\
    if (list->size == 0)\
        list->first = new_node;\
    if (list->last != NULL)\
        list->last->next = new_node;\
    list->last = new_node;\
    list->size++;\
    return value;\
}\
T list_##T##_push_front(list_##T* list, T value)\
{\
    if (list == NULL)\
        return (T)NOVAL;\
    node_##T* new_node = node_##T##_create(NULL, list->first, value);\
    if (list->size == 0)\
        list->last = new_node;\
    list->first = new_node;\
    list->size++;\
    return value;\
}\
T list_##T##_pop_back(list_##T* list)\
{\
    if (list == NULL || list->size == 0)\
        return (T)NOVAL;\
    T pop_val = list->last->value;\
    list->size--;\
    node_##T* old_node = list->last;\
    if (list->size > 1)\
    {\
        list->last = list->last->prev;\
    }\
    else\
    {\
        list->first = NULL;\
        list->last = NULL;\
    }\
    node_##T##_free(old_node);\
    return pop_val;\
}\
T list_##T##_pop_front(list_##T* list)\
{\
    if (list == NULL || list->size == 0)\
        return (T)NOVAL;\
    T pop_val = list->first->value;\
    list->size--;\
    node_##T* old_node = list->first;\
    if (list->size > 1)\
    {\
        list->first = list->first->prev;\
    }\
    else\
    {\
        list->first = NULL;\
        list->last = NULL;\
    }\
    node_##T##_free(old_node);\
    return pop_val;\
}\
T list_##T##_insert(list_##T* list, uint32_t idx, T value)\
{\
    if (list == NULL)\
        return (T)NOVAL;\
    if (idx == 0)\
        return list_##T##_push_front(list, value);\
    else if (idx == list->size - 1)\
        return list_##T##_push_back(list, value);\
    uint32_t current_idx;\
    node_##T* current_node;\
    if (idx <= list->size / 2)\
    {\
        current_node = list->first;\
        current_idx = 0;\
        while(current_idx != idx)\
        {\
            current_node = current_node->next;\
            current_idx++;\
        }\
    }\
    else\
    {\
        current_node = list->last;\
        current_idx = list->size - 1;\
        while(current_idx != idx)\
        {\
            current_node = current_node->prev;\
            current_idx--;\
        }\
    }\
    node_##T* new_node = node_##T##_create(current_node->prev, current_node, value);\
    current_node->prev->next = new_node;\
    current_node->prev = new_node;\
    list->size++;\
    return value;\
}\
T list_##T##_remove(list_##T* list, uint32_t idx)\
{\
    if (list == NULL)\
        return (T)NOVAL;\
    if (idx == 0U)\
        return list_##T##_pop_front(list);\
    else if (idx == list->size - 1)\
        return list_##T##_pop_back(list);\
    list->size++;\
    uint32_t current_idx;\
    node_##T* current_node;\
    if (idx <= list->size / 2)\
    {\
        current_node = list->first;\
        current_idx = 0;\
        while(current_idx != idx)\
        {\
            current_node = current_node->next;\
            current_idx++;\
        }\
    }\
    else\
    {\
        current_node = list->last;\
        current_idx = list->size - 1;\
        while(current_idx != idx)\
        {\
            current_node = current_node->prev;\
            current_idx--;\
        }\
    }\
    current_node->prev->next = current_node->next;\
    current_node->next->prev = current_node->prev;\
    node_##T##_free(current_node);\
    return current_node->value;\
}\
T list_##T##_get(list_##T* list, uint32_t idx)\
{\
    if (list == NULL)\
        return (T)NOVAL;\
    if (idx >= list->size)\
        return (T)NOVAL;\
    uint32_t current_idx = 0;\
    node_##T* current_node;\
    if (idx <= list->size  / 2)\
    {\
        current_node = list->first;\
        current_idx = 0;\
        while(current_idx != idx)\
        {\
            current_node = current_node->next;\
            current_idx++;\
        }\
    }\
    else\
    {\
        current_node = list->last;\
        current_idx = list->size - 1;\
        while(current_idx != idx)\
        {\
            current_node = current_node->prev;\
            current_idx--;\
        }\
    }\
    return current_node->value;\
}\







#endif
